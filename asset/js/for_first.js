$('.question_title').on('click',function(e){
    let id = $(this).attr('id');
    let str = 'hidden'
    if(($('#'+id+' .answer').attr('class')).search(str)===-1){
        $('#'+id+' .answer').addClass('hidden');
        $('#'+id+' .fa-angle-up').addClass('hidden');
        $('#'+id+' .fa-angle-down').removeClass('hidden')
    }
    else{
        $('#'+id+' .answer').removeClass('hidden');
        $('#'+id+' .fa-angle-up').removeClass('hidden');
        $('#'+id+' .fa-angle-down').addClass('hidden')

    }
})
$('.work').click(function(e){
    e.preventDefault();
    var e = $(this);
    var image = e.data('image');
    var body = e.data('value');
    $('.modal-content .title img').attr("src","asset/img/items/"+image);
    $('.modal-content .title .job').html(jQuery(".work_title", this).html());
    $('.modal-content .title .job').append(`<p>${jQuery(".work_title", this).html()}のお仕事詳細について</p>`);
    console.log()
});