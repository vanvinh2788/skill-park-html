/*==================== MENU SHOW Y HIDDEN ====================*/


/*===== MENU SHOW =====*/
/* Validate if constant exists */


/*===== MENU HIDDEN =====*/
/* Validate if constant exists */


$(document).ready(function(){
    var isNavbar= true;
    /*====================  NAVBAR ====================*/

    /*-------------Scroll----------*/
    $(window).on("scroll",function(){
        
        if($(this).scrollTop() >90 && isNavbar == true){
            $(".header").addClass("navbar-scroll")
            isNavbar = false;
        }else if($(this).scrollTop() < 9 && isNavbar == false) {
            setTimeout(function(){
                $(".header").removeClass("navbar-scroll")
                isNavbar=true;
            },100)
        }
    })
    /*-------------Click menu----------*/

    $(".header-menu").on('click',function(){
        $(".header-nav").toggleClass('header-nav--show');
        // $(".btn--signin--close").toggleClass('hidden')
        $(".header-account").toggleClass('hidden-0')
        $(".btn--sign-in--close").toggleClass('hidden')
        $(".header-menu__div").toggleClass('menu--hidden')
        $(".header-menu__close").toggleClass('menu--show')
    })
    $(".header-account__img").on('click',function(){
        $(".account-box").toggleClass('hidden');
    })

    /*==================== COLLAGE PAGE EDITPROFILE ====================*/
    var acc = document.querySelectorAll(".accordion-item");
    var acc_title = document.querySelectorAll(".accordion-title");
    var i;

    acc_title.forEach((tab,index) => {
        tab.onclick= function(){
            this.classList.toggle('active')
            acc[index].classList.toggle('active')
        }
    })

        var height = document.getElementsByClassName("accordion-content").offsetHeight;
        console.log(height);


/*==================== DAY FOR SELECT ====================*/
    const monthNames = ["1", "2", "3", "4", "5", "6",
    "7", "8", "9", "10", "11", "12"
    ];
    let qntYears = 25;
    let selectYear = $(".year");
    let selectMonth = $(".month");
    let selectDay = $("#day");
    let currentYear = new Date().getFullYear();

    for (var y = 0; y < qntYears; y++) {
        let date = new Date(currentYear);
        let yearElem = document.createElement("option");
        yearElem.value = currentYear
        yearElem.textContent = currentYear;
        selectYear.append(yearElem);
        currentYear--;
    }

    for (var m = 0; m < 12; m++) {
        let month = monthNames[m];
        let monthElem = document.createElement("option");
        monthElem.value = m;
        monthElem.textContent = month;
        selectMonth.append(monthElem);
    }

    var d = new Date();
    var month = d.getMonth();
    var year = d.getFullYear();
    var day = d.getDate();

    selectYear.val(year);
    selectYear.on("change", AdjustDays);
    selectMonth.val(month);
    selectMonth.on("change", AdjustDays);

    AdjustDays();
    selectDay.val(day)

    function AdjustDays() {
    var year = selectYear.val();
    var month = parseInt(selectMonth.val()) + 1;
    selectDay.empty();

    //get the last day, so the number of days in that month
    var days = new Date(year, month, 0).getDate();

    //lets create the days of that month
    for (var d = 1; d <= days; d++) {
        var dayElem = document.createElement("option");
        dayElem.value = d;
        dayElem.textContent = d;
        selectDay.append(dayElem);
    }
    }

/*==================== DIALOG JOBBOARD ====================*/
/*-------JobBoard Page----------*/
$(".jobBoard .recruit-list").on('click',function(e){
    if(!e.target.closest(".recruit-list__button")){
        $(".modal").addClass('show');
        $(".backgroundBlur").removeClass('hidden');
        $('html, body').css({
            overflow: 'hidden',
        });
    }
})
$('.jobBoard .modal').on('click', function (e) {
    if(!e.target.closest(".jobBoard-dialog")){
        $(".modal").removeClass('show');
        $(".backgroundBlur").addClass('hidden');
        $('html, body').css({
            overflow: 'auto',
        });
    }
});
$('.jobBoard .jobBoard-dialog__close').on('click', function () {
        $(".modal").removeClass('show');
        $(".backgroundBlur").addClass('hidden');
        $('html, body').css({
            overflow: 'auto',
        });
})
/*-------toppae Page----------*/
$(".topPage .recruit-list").on('click',function(e){
    if(!e.target.closest(".recruit-list__button")){
        $(".topPage .modal").addClass('show');
        $(".topPage .backgroundBlur").removeClass('hidden');
        $('html, body').css({
            overflow: 'hidden',
        });
    }
})
$('.topPage .modal').on('click', function (e) {
    if(!e.target.closest(".topPage-dialog")){
        $(".topPage .modal").removeClass('show');
        $(".topPage .backgroundBlur").addClass('hidden');
        $('html, body').css({
            overflow: 'auto',
        });
    }
});
$('.topPage .topPage-dialog__close').on('click', function () {
        $(".topPage .modal").removeClass('show');
        $(".topPage .backgroundBlur").addClass('hidden');
        $('html, body').css({
            overflow: 'auto',
        });
})
/*-------------DIALOG USER-----------------*/
$('.topPage .recruit-list__button').on('click', function () {
    $(".topPage .dialog-user").removeClass('hidden');
    $(".topPage .backgroundBlur").removeClass('hidden');
})
$('.topPage .dialog-user__close').on('click', function () {
    $(".topPage .dialog-user").addClass('hidden');
    $(".topPage .backgroundBlur").addClass('hidden');
})
$('.topPage .backgroundBlur').on('click', function () {
    $(".topPage .dialog-user").addClass('hidden');
    $(".topPage .backgroundBlur").addClass('hidden');
})
/*==================== PORTFOLIO SWIPER  ====================*/


/*==================== TESTIMONIAL ====================*/


/*==================== SCROLL SECTIONS ACTIVE LINK ====================*/


/*==================== CHANGE BACKGROUND HEADER ====================*/ 


/*==================== SHOW SCROLL UP ====================*/ 


/*==================== DARK LIGHT THEME ====================*/ 
})
